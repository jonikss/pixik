export const TOGGLES_SET_SIZE = 'FILTER_SET_SIZE';
export const TOGGLES_SET_FILTER = 'TOGGLES_SET_FILTER';

export const setSize = size => {
    return {
        type: TOGGLES_SET_SIZE,
        payload: size
    };
}

export const setFilter = filter => {
    return {
        type: TOGGLES_SET_FILTER,
        payload: filter
    };
}

