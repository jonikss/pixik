import React, { Component } from 'react';
import { string, array, bool, func } from 'prop-types'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'

import * as categoriesActions from '../../actions/categories';
import './index.css';

class Categories extends Component {

    static propTypes = {
        fetchCategories: func.isRequired,
        items: array.isRequired,
        error: bool.isRequired,
        errorMsg: string.isRequired,
        loading: bool.isRequired
    }

    static defaultProps = {
        items: [],
        error: false,
        errorMsg: '',
        loading: false
    }

    componentWillMount() {
        if (this.props.items.length === 0) {
            this.props.fetchCategories();
        }
    }

    clickOnLink = id => e => {
        e.preventDefault();
        const { history, match } = this.props;
        if(match.params.category === id) {
            history.push('/');
        } else {
            history.push(`/${id}/`);
        }
    }

    render() {
        const { items, loading, match } = this.props;
        let selectedId = '';
        if (match.params.category) {
            selectedId = match.params.category;
        }
        return <div className='categories'>
            {loading ? <p>Идет загрузка</p> : ''}
            <ul className='categories__items'>
                {
                    items && items.length > 0
                        ?
                        items.map(item =>
                            <li 
                            key={item.id} 
                            className={'categories__item' + (selectedId === item.id ? ' categories__item--selected' : '')}>
                                <a onClick={
                                    this.clickOnLink(item.id)}
                                    className='categories__item-link'
                                >
                                    {item.name}
                                </a>
                            </li>)
                        :
                        ''
                }
            </ul>
        </div>;
    }
}

export default withRouter(connect(
    (state, props) => state.categories,
    dispatch => ({
        fetchCategories: bindActionCreators(categoriesActions, dispatch).fetchCategories,
    })
)(Categories));
