import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import './index.css';

class Result extends Component {

    getImageUrl = () => {
        let { filter, size, match } = this.props;
        let category = 'any';
        if (match.params.category) {
            category = match.params.category;
        }
        size = size.replace('x','/');
        let result = `https://placeimg.com/${size}/${category}`;
        if(filter !== ''){
            result = result + `/${filter}`;
        }

        return result;
    }

    render() {
        return <div className='result'>
            <img src={this.getImageUrl()} className='result__img' alt='' />
            <p>{this.getImageUrl()}</p>
        </div>;
    }
}

export default withRouter(connect(
    (state, props) => state.toggles
)(Result));
