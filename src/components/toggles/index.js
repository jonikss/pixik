import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as togglesActions from '../../actions/toggles';
import './index.css';

class Toggles extends Component {

    hundlerSetSize = sizes => e => {
        e.preventDefault();
        const { setSize } = this.props;
        setSize(sizes);
    }

    hundlerSetFilter = selectedFilter => e => {
        e.preventDefault();
        const { setFilter, filter } = this.props;
        if(selectedFilter === filter) {
            setFilter('');
        } else {
            setFilter(selectedFilter);
        }
    }

    render() {
        const { filter, size } = this.props;
        const sizesArray = ['400x400', '800x600', '1920x1080'];
        const filterArray = ['grayscale', 'sepia'];
        return <div className='toggles'>
            <div className='toggles__sizes'>
                {
                    sizesArray.map(sizeItem =>
                        <a
                            key={sizeItem}
                            onClick={this.hundlerSetSize(sizeItem)}
                            className={'toggles__size' + (size === sizeItem ? ' toggles__size--selected' : '')}>
                            {sizeItem}
                        </a>
                    )
                }
            </div>
            <div className='toggles__filters'>
                {
                    filterArray.map(filterItem =>
                        <a
                            key={filterItem}
                            onClick={this.hundlerSetFilter(filterItem)}
                            className={'toggles__filter' + (filter === filterItem ? ' toggles__filter--selected' : '')}>
                            {filterItem}
                        </a>
                    )
                }
            </div>
        </div>;
    }
}

export default connect(
    (state, props) => state.toggles,
    dispatch => ({
        setFilter: bindActionCreators(togglesActions, dispatch).setFilter,
        setSize: bindActionCreators(togglesActions, dispatch).setSize,
    })
)(Toggles);
