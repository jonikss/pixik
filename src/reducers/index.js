import categories from './categories';
import toggles from './toggles';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    categories,
    toggles
});

export default rootReducer;
