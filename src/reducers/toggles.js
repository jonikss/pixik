import { TOGGLES_SET_SIZE, TOGGLES_SET_FILTER } from '../actions/toggles';

const initialState = {
    filter: '',
    size: '400x400'
}

export default (state = initialState, {type, payload, error}) => {
    switch (type) {
        case TOGGLES_SET_SIZE:
            return {...state, size: payload};
        case TOGGLES_SET_FILTER:
            return {...state, filter: payload};
        default:
            return state;
    }
};