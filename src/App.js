import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Categories from './components/categories'
import Toggles from './components/toggles'
import Result from './components/result'
import './app.css'


class App extends Component {
    render() {
        return (
            <main className='app'>
                <div className='app__wrap'>
                <div className='app__row'>
                    <div className='app__left'>
                        <Switch>
                            <Route
                                path={'/:category/'}
                                component={Categories}
                            />
                            <Route path={'/'} component={Categories} />
                        </Switch>
                    </div>
                    <div className='app__right'>
                        <Toggles/>
                        <Switch>
                            <Route
                                path={'/:category/'}
                                component={Result}
                            />
                            <Route path={'/'} component={Result} />
                        </Switch>
                    </div>
                </div>
                </div>
            </main>
        );
    }
}

export default App;
