const getCategories = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve([
                {
                    id:'animals',
                    name:'Animals'
                },
                {
                    id:'arch',
                    name:'Architecture'
                },
                {
                    id:'nature',
                    name:'Nature'
                },
                {
                    id:'people',
                    name:'People'
                },
                {
                    id:'tech',
                    name:'Tech'
                }
            ]);
        }, 3000);
      });
}

export default getCategories;